/* Nama : Ryan Fadhillah
   NIM  : 20/457221/SV/17668
   Calon Programmer Heroes */

//library untuk lcd
#include <LiquidCrystal.h>

//deklarasi pin yang digunakan pada arduino
#define trigPin 8
#define echoPin 9
#define buzzPin 6
#define led1Pin 13
#define led2Pin 7
#define led3Pin 10

long duration, distance;
LiquidCrystal lcd(12,11,5,4,3,2);

void setup() {
  lcd.begin(16,2);
  pinMode(echoPin,INPUT);
  pinMode(trigPin,OUTPUT);
  pinMode(buzzPin,OUTPUT);
  pinMode(led1Pin,OUTPUT);
  pinMode(led2Pin,OUTPUT);
  pinMode(led3Pin,OUTPUT);
}

void loop(){
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = duration*0.034/2;
  
  if (distance <= 60) {
    digitalWrite(led1Pin, HIGH);
  }
  else {
    digitalWrite(led1Pin,LOW);
  }
  
  if(distance > 60 && distance <= 150){
    digitalWrite(led2Pin, HIGH);
  }
  else {
    digitalWrite(led2Pin,LOW);
  }
  
  if (distance >= 150) {
    digitalWrite(led3Pin, HIGH);
  }
  else {
    digitalWrite(led3Pin,LOW);
  }
  
  if(distance <= 40){
    tone(buzzPin,1000);   
  }
  if(distance > 40 && distance <= 60){
    buzzOn(200,50);
  }
  if(distance > 60 && distance <= 80){
    buzzOn(300,100);
  }
  if(distance > 80 && distance <= 110){
    buzzOn(400,200);
  }
  if(distance > 110 && distance <= 140){
    buzzOn(500,300);
  }
  if(distance > 140 && distance <= 200){
    buzzOn(700,400);
  }
  if(distance > 200){
    noTone(buzzPin);
  }
  lcd.setCursor(0,0);
  lcd.print("  Jarak: ");
  lcd.print(distance); 
  lcd.print(" cm  ");

  delay(5);
}
  
void buzzOn(int on, int off){
  tone(buzzPin,1000);
  delay(on);
  noTone(buzzPin);
  delay(off);
 }
